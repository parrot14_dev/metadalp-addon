let native = browser.runtime.connectNative("metadalp_bridge");

native.onMessage.addListener((response) => {
  console.log(`Received: ${response}`);
});


browser.action.onClicked.addListener(async (tab) => {
  let response = await browser.tabs.sendMessage(tab.id, { type: "extract" });

  if(response){
    native.postMessage(response);
  }
});