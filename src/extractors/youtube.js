browser.runtime.onMessage.addListener(async (data, sender) => {
  if (data.type === "extract") {
    const params = new URLSearchParams(window.location.search);

    if (!params.has("v")){
      return false;
    }

    const url = params.get("v");

    const response = await fetch(`https://www.youtube.com/oembed?format=json&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3D${url}`, {
      referrerPolicy: "no-referrer"
    });
    const data = await response.json();

    return {
      title: data.title,
      domain: "www.youtube.com",
      url: url
    };
  }
  return false;
});  

console.log("ready");